from material import *
from source import *
from photons import *
from ct_detect import *
from fake_source import *
from ct_phantom import *
from ct_lib import *
from ct_scan import *
from ct_calibrate import *
from back_project import *
from scan_and_reconstruct import *
from create_dicom import *
from xtreme import *
import matplotlib.pyplot as plt
import numpy as np
import scipy
import math
import pydicom
import numpy.matlib
import pickle

#######create object instances#######
material = Material()
source = Source()


def hu(p, material, reconstruction, scale):
	""" convert CT reconstruction output to Hounsfield Units
	calibrated = hu(p, material, reconstruction, scale) converts the reconstruction into Hounsfield
	Units, using the material coefficients, photon energy p and scale given."""

	# use water to calibrate
	
	water_scan =  sum(photons (p, material.coeff('Water'), 1))

	# put this through the same calibration process as the normal CT data
	
	n = reconstruction.shape[1]
	water_calib=ct_calibrate(p, material, water_scan * np.ones((1,n)), 0.01)
	
	# use result to convert to hounsfield units
	
	houns_y=((reconstruction - water_calib)/water_calib)*1000
	
	# limit minimum to -1024, which is normal for CT data.
	
	houns_y[houns_y < -1024] = -1024
	reconstruction = houns_y

	return reconstruction