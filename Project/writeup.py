import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
import os

## General file location
fileloc = "/media/rob/C24CDC504CDC40B9/Users/Rob Sumner/Documents/Work/University/Third Year/GG2 CT Project/"

# ## Plot attenuation coefficients
# df = pd.read_excel("mass_attenuation_coeffs.xls", sheet_name="Materials")
# colnames = list(df)
# mev = df[colnames[0]]

# for material in colnames[2::2]:
#     plt.plot(mev, df[material], label=material)
# plt.rc('text', usetex=True)
# plt.title("Plot of Photon Energy against Attenuation Coefficient")
# plt.legend()
# plt.xlabel("Photon Energy (MeV)")
# plt.ylabel("Attenuation Coefficient $\mu$ $(cm^{-1})$")
# plt.yscale('log')
# plt.xscale('log')
# #plt.xlim([0.001,0.01])
# #plt.ylim([0.001,10000])
# #plt.savefig(fileloc + "Writeup/Images/image.png")
# plt.show()

## Check difference between image plots. 
output0 = pickle.load( open( fileloc+"Files/sino3_Angle64_Order0.p", "rb" ) )
output1 = pickle.load( open( fileloc+"Files/sino3_Angle64_Order1.p", "rb" ) )
output3 = pickle.load( open( fileloc+"Files/sino3_Angle64_Order3.p", "rb" ) )
# Scale values
output0 -= output0.min()
output0 /= output0.max()
output1 -= output1.min()
output1 /= output1.max()
output3 -= output3.min()
output3 /= output3.max()
plt.plot(np.linspace(0,len(output0[32,:]), len(output0[32,:])), output0[16,:], label="Nearest Neighbour")
plt.plot(np.linspace(0,len(output1[32,:]), len(output1[32,:])), output1[16,:], label="Linear")
plt.plot(np.linspace(0,len(output3[32,:]), len(output3[32,:])), output3[16,:], label="Cubic")
plt.title("Plot of Sinogram Pixel Values for Different Interpolation Methods")
plt.xlabel("Pixel Position")
plt.ylabel("Pixel Value")
plt.legend()
plt.savefig(fileloc + "Writeup/Images/interpolationComp.png")
plt.show()

