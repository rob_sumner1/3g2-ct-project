import numpy as np
import scipy
from scipy import interpolate
from ct_lib import *
from ct_detect import *


def ct_calibrate(photons, material, sinogram, scale, correct=True):
    """ ct_calibrate convert CT detections to linearised attenuation
    sinogram = ct_calibrate(photons, material, sinogram, scale) takes the CT detection sinogram
    in x (angles x samples) and returns a linear attenuation sinogram
    (angles x samples). photons is the source energy distribution, material is the
    material structure containing names, linear attenuation coefficients and
    energies in mev, and scale is the size of each pixel in x, in cm."""

    # Get dimensions and work out detection for just air of twice the side
    # length (has to be the same as in ct_scan.m)
    n = sinogram.shape[1]  # Sinogram is #scans x #pixels

    # Air attenuated values to normalise against
    coeffs = material.coeff('Air')
    depth_vals = 2*n*scale

    # Find attenuated intensities in air only
    air_out = ct_detect(photons, coeffs, depth_vals)

    # Beam hardening correction for water
    # perform scan of water at varying depths
    points = 1000
    t_w = np.linspace(0, points, points)
    water_scan = ct_detect(photons, material.coeff('Water'), t_w)

    # clip the water scan when it reaches one photon
    one = water_scan[-1]
    water_scan = list(filter(lambda a: a != one, water_scan))
    t_w = t_w[:len(water_scan)]

    # find water attenuations and fit function
    mu_w = -np.log(water_scan/air_out[0])
    degree = 5
    p_coeffs = np.polyfit(mu_w, t_w, deg=degree)

    # construct the fitted function
    def f(x):
        y = 0
        for i in range(degree+1):
            y += p_coeffs[i] * (x**(degree-i))
        return y

    # Normalise sinogram data
    attenuation = np.zeros(sinogram.shape)

    # Set the constant C
    C = 0.2632

    if correct is False:
        print('No beam hardening correction.')
        for i in range(sinogram.shape[0]):
            attenuation[i, :] = -np.log(sinogram[i, :]/air_out[0])
    else:
        print('Beam hardening correction for water.')
        for i in range(sinogram.shape[0]):
            mu_m = -np.log(sinogram[i, :]/air_out[0])
            t_wm = np.array([f(x) for x in mu_m])
            mu_c = C * t_wm
            attenuation[i, :] = mu_c

    return attenuation
