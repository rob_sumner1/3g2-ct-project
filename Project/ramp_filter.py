import math
import numpy as np
import matplotlib.pyplot as plt


def ramp_filter(sinogram, scale, alpha=0.001):
    """ Ram-Lak filter with raised-cosine for CT reconstruction

    fs = ramp_filter(sinogram, scale) filters the input in sinogram (angles x samples)
    using a Ram-Lak filter.

    fs = ramp_filter(sinogram, scale, alpha) can be used to modify the Ram-Lak filter by a
    cosine raised to the power given by alpha."""

    # get input dimensions
    angles = sinogram.shape[0]
    n = sinogram.shape[1]

    # Set up filter to be at least twice as long as input
    m = np.floor(np.log(n) / np.log(2) + 2)
    m = int(2 ** m)

    # Build ramlak filter coefficients based on m
    # Filter corresponds to outputs from FFT: [0 to wmax, -wmax to 0]
    wmax = np.pi/scale
    dw = 2*wmax/(m+2)
    ramlak = np.arange(1, (m/2)+1, 1)*dw/(2*np.pi)
    ramlak = np.concatenate( (np.array([dw/(8*np.pi)]), ramlak, ramlak[::-1][1::]), axis=0)

    # Add raised cosine based on alpha value provided.
    ramlak *= np.abs(np.cos(np.linspace(0, m, m)*np.pi/m)) ** alpha

    # Apply filter to all angles
    # Use duality of multiplication and convolution in frequency domain.
    print('Ramp filtering...')
    filt_sinogram = np.zeros(sinogram.shape)
    for i in range(angles):
        sino_FFT = np.fft.fft(sinogram[i][:], m)
        filt_sinogram[i][:] = np.real(np.fft.ifft(sino_FFT*ramlak))[:n]

    # # Plot values.
    # impulse = np.fft.ifft(ramlak)
    # impulse = np.concatenate( (impulse[128::], impulse[0:128]), axis=0)
    # plt.rc('text', usetex=True)
    # plt.rc('font', family='serif')
    # fig, axs = plt.subplots(2)

    # axs[1].plot(np.linspace(-m/2,m/2,m),ramlak)
    # axs[1].set_xlabel("Frequency \div \Delta\omega}")
    # axs[1].set_ylabel("Filter Coefficient")

    # axs[0].plot(np.linspace(-128,128,256), impulse[-256::])
    # axs[0].set_ylabel('Impulse Response')
    # axs[0].set_title("Ram-Lak Filter")
    # # plt.savefig("ImpulseResponse.png")
    # plt.show()

    return filt_sinogram
